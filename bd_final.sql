--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.10
-- Dumped by pg_dump version 10.2

-- Started on 2018-11-17 09:42:54

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 2198 (class 0 OID 0)
-- Dependencies: 2197
-- Name: DATABASE pratica1_bd; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON DATABASE pratica1_bd IS 'banco de dados da pratica 1';


--
-- TOC entry 1 (class 3079 OID 12387)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2200 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 185 (class 1259 OID 41054)
-- Name: categoria; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE categoria (
    idcategoria integer NOT NULL,
    nome character varying(100) NOT NULL
);


ALTER TABLE categoria OWNER TO postgres;

--
-- TOC entry 186 (class 1259 OID 41057)
-- Name: categoria_idcategoria_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE categoria_idcategoria_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE categoria_idcategoria_seq OWNER TO postgres;

--
-- TOC entry 2201 (class 0 OID 0)
-- Dependencies: 186
-- Name: categoria_idcategoria_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE categoria_idcategoria_seq OWNED BY categoria.idcategoria;


--
-- TOC entry 187 (class 1259 OID 41059)
-- Name: cliente; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE cliente (
    codigo integer NOT NULL,
    nome character varying(100) NOT NULL,
    cpf integer,
    endereco character varying(100),
    telefone integer
);


ALTER TABLE cliente OWNER TO postgres;

--
-- TOC entry 188 (class 1259 OID 41062)
-- Name: cliente_idcliente_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE cliente_idcliente_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cliente_idcliente_seq OWNER TO postgres;

--
-- TOC entry 2202 (class 0 OID 0)
-- Dependencies: 188
-- Name: cliente_idcliente_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE cliente_idcliente_seq OWNED BY cliente.codigo;


--
-- TOC entry 189 (class 1259 OID 41064)
-- Name: condicional; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE condicional (
    idcondicional integer NOT NULL,
    produto integer NOT NULL
);


ALTER TABLE condicional OWNER TO postgres;

--
-- TOC entry 190 (class 1259 OID 41067)
-- Name: condicional_idcondicional_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE condicional_idcondicional_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE condicional_idcondicional_seq OWNER TO postgres;

--
-- TOC entry 2203 (class 0 OID 0)
-- Dependencies: 190
-- Name: condicional_idcondicional_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE condicional_idcondicional_seq OWNED BY condicional.idcondicional;


--
-- TOC entry 191 (class 1259 OID 41069)
-- Name: produto; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE produto (
    idproduto integer NOT NULL,
    descricao character varying(100) NOT NULL,
    custo numeric(10,2) NOT NULL,
    observacao character varying(250) NOT NULL,
    imagem character varying(250) NOT NULL,
    preco numeric(10,2),
    "idCategoria" integer
);


ALTER TABLE produto OWNER TO postgres;

--
-- TOC entry 192 (class 1259 OID 41075)
-- Name: produto_idproduto_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE produto_idproduto_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE produto_idproduto_seq OWNER TO postgres;

--
-- TOC entry 2204 (class 0 OID 0)
-- Dependencies: 192
-- Name: produto_idproduto_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE produto_idproduto_seq OWNED BY produto.idproduto;


--
-- TOC entry 193 (class 1259 OID 41077)
-- Name: recebimento; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE recebimento (
    idrecebimento integer NOT NULL,
    valor numeric(8,2) NOT NULL,
    datarecebimento date NOT NULL
);


ALTER TABLE recebimento OWNER TO postgres;

--
-- TOC entry 194 (class 1259 OID 41080)
-- Name: recebimento_idrecebimento_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE recebimento_idrecebimento_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE recebimento_idrecebimento_seq OWNER TO postgres;

--
-- TOC entry 2205 (class 0 OID 0)
-- Dependencies: 194
-- Name: recebimento_idrecebimento_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE recebimento_idrecebimento_seq OWNED BY recebimento.idrecebimento;


--
-- TOC entry 195 (class 1259 OID 41082)
-- Name: usuario; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE usuario (
    idusuario integer NOT NULL,
    status integer NOT NULL,
    nome character varying(150) NOT NULL,
    usuario character varying(150) NOT NULL,
    senha character varying(150) NOT NULL,
    nivelpermissao integer NOT NULL,
    email character varying(150) NOT NULL
);


ALTER TABLE usuario OWNER TO postgres;

--
-- TOC entry 196 (class 1259 OID 41088)
-- Name: usuario_idusuario_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE usuario_idusuario_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE usuario_idusuario_seq OWNER TO postgres;

--
-- TOC entry 2206 (class 0 OID 0)
-- Dependencies: 196
-- Name: usuario_idusuario_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE usuario_idusuario_seq OWNED BY usuario.idusuario;


--
-- TOC entry 197 (class 1259 OID 41090)
-- Name: venda; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE venda (
    idvenda integer NOT NULL,
    valortotal numeric(5,2),
    observacoes character varying(250) NOT NULL,
    saldo integer NOT NULL,
    data date
);


ALTER TABLE venda OWNER TO postgres;

--
-- TOC entry 198 (class 1259 OID 41093)
-- Name: venda_idvenda_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE venda_idvenda_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE venda_idvenda_seq OWNER TO postgres;

--
-- TOC entry 2207 (class 0 OID 0)
-- Dependencies: 198
-- Name: venda_idvenda_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE venda_idvenda_seq OWNED BY venda.idvenda;


--
-- TOC entry 2039 (class 2604 OID 41095)
-- Name: categoria idcategoria; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY categoria ALTER COLUMN idcategoria SET DEFAULT nextval('categoria_idcategoria_seq'::regclass);


--
-- TOC entry 2040 (class 2604 OID 41096)
-- Name: cliente codigo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cliente ALTER COLUMN codigo SET DEFAULT nextval('cliente_idcliente_seq'::regclass);


--
-- TOC entry 2041 (class 2604 OID 41097)
-- Name: condicional idcondicional; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY condicional ALTER COLUMN idcondicional SET DEFAULT nextval('condicional_idcondicional_seq'::regclass);


--
-- TOC entry 2042 (class 2604 OID 41098)
-- Name: produto idproduto; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY produto ALTER COLUMN idproduto SET DEFAULT nextval('produto_idproduto_seq'::regclass);


--
-- TOC entry 2043 (class 2604 OID 41099)
-- Name: recebimento idrecebimento; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY recebimento ALTER COLUMN idrecebimento SET DEFAULT nextval('recebimento_idrecebimento_seq'::regclass);


--
-- TOC entry 2044 (class 2604 OID 41100)
-- Name: usuario idusuario; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY usuario ALTER COLUMN idusuario SET DEFAULT nextval('usuario_idusuario_seq'::regclass);


--
-- TOC entry 2045 (class 2604 OID 41101)
-- Name: venda idvenda; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY venda ALTER COLUMN idvenda SET DEFAULT nextval('venda_idvenda_seq'::regclass);


--
-- TOC entry 2179 (class 0 OID 41054)
-- Dependencies: 185
-- Data for Name: categoria; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO categoria (idcategoria, nome) VALUES (4, 'camisetas');
INSERT INTO categoria (idcategoria, nome) VALUES (7, 'aa');


--
-- TOC entry 2181 (class 0 OID 41059)
-- Dependencies: 187
-- Data for Name: cliente; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO cliente (codigo, nome, cpf, endereco, telefone) VALUES (9, 'Mateus', 2134625082, 'RR', 0);


--
-- TOC entry 2183 (class 0 OID 41064)
-- Dependencies: 189
-- Data for Name: condicional; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2185 (class 0 OID 41069)
-- Dependencies: 191
-- Data for Name: produto; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2187 (class 0 OID 41077)
-- Dependencies: 193
-- Data for Name: recebimento; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2189 (class 0 OID 41082)
-- Dependencies: 195
-- Data for Name: usuario; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2191 (class 0 OID 41090)
-- Dependencies: 197
-- Data for Name: venda; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2208 (class 0 OID 0)
-- Dependencies: 186
-- Name: categoria_idcategoria_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('categoria_idcategoria_seq', 7, true);


--
-- TOC entry 2209 (class 0 OID 0)
-- Dependencies: 188
-- Name: cliente_idcliente_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('cliente_idcliente_seq', 10, true);


--
-- TOC entry 2210 (class 0 OID 0)
-- Dependencies: 190
-- Name: condicional_idcondicional_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('condicional_idcondicional_seq', 1, false);


--
-- TOC entry 2211 (class 0 OID 0)
-- Dependencies: 192
-- Name: produto_idproduto_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('produto_idproduto_seq', 1, false);


--
-- TOC entry 2212 (class 0 OID 0)
-- Dependencies: 194
-- Name: recebimento_idrecebimento_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('recebimento_idrecebimento_seq', 1, false);


--
-- TOC entry 2213 (class 0 OID 0)
-- Dependencies: 196
-- Name: usuario_idusuario_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('usuario_idusuario_seq', 1, false);


--
-- TOC entry 2214 (class 0 OID 0)
-- Dependencies: 198
-- Name: venda_idvenda_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('venda_idvenda_seq', 1, false);


--
-- TOC entry 2047 (class 2606 OID 41103)
-- Name: categoria categoria_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY categoria
    ADD CONSTRAINT categoria_pkey PRIMARY KEY (idcategoria);


--
-- TOC entry 2049 (class 2606 OID 41105)
-- Name: cliente cliente_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cliente
    ADD CONSTRAINT cliente_pkey PRIMARY KEY (codigo);


--
-- TOC entry 2051 (class 2606 OID 41107)
-- Name: condicional condicional_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY condicional
    ADD CONSTRAINT condicional_pkey PRIMARY KEY (idcondicional);


--
-- TOC entry 2053 (class 2606 OID 41109)
-- Name: produto produto_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY produto
    ADD CONSTRAINT produto_pkey PRIMARY KEY (idproduto);


--
-- TOC entry 2055 (class 2606 OID 41111)
-- Name: recebimento recebimento_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY recebimento
    ADD CONSTRAINT recebimento_pkey PRIMARY KEY (idrecebimento);


--
-- TOC entry 2057 (class 2606 OID 41113)
-- Name: usuario usuario_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY usuario
    ADD CONSTRAINT usuario_pkey PRIMARY KEY (idusuario);


--
-- TOC entry 2059 (class 2606 OID 41115)
-- Name: venda venda_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY venda
    ADD CONSTRAINT venda_pkey PRIMARY KEY (idvenda);


--
-- TOC entry 2060 (class 2606 OID 49178)
-- Name: condicional idproduto_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY condicional
    ADD CONSTRAINT idproduto_fkey FOREIGN KEY (produto) REFERENCES produto(idproduto);


--
-- TOC entry 2061 (class 2606 OID 41116)
-- Name: venda idusuario; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY venda
    ADD CONSTRAINT idusuario FOREIGN KEY (idvenda) REFERENCES usuario(idusuario);


-- Completed on 2018-11-17 09:42:54

--
-- PostgreSQL database dump complete
--

