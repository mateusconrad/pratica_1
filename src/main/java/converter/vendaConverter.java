/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package converter;

import dao.VendaDAO;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import model.Venda;

@FacesConverter("vendaConverter")
public class vendaConverter implements Converter{
    
    private VendaDAO vendaDAO = new VendaDAO();
    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String string) {
        return vendaDAO.findById(Integer.parseInt(string));
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object o) {
       if(o != null){
        return "" + ( ( Venda ) o ).getIdVenda();
       } else{
           return "";
       }
    }
    
}
