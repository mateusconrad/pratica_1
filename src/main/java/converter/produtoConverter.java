package converter;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import model.Produto;
import dao.ProdutoDAO;

@FacesConverter("produtoConverter")
public class produtoConverter implements Converter{
    
    private ProdutoDAO produtoDAO = new ProdutoDAO();
    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String string) {
        return produtoDAO.findById(Integer.parseInt(string));
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object o) {
         return "" + ( ( Produto ) o ).getIdProduto();
    }
    
}
