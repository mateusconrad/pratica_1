
package converter;

import dao.CategoriaDAO;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import model.Categoria;


@FacesConverter("categoriaConverter")
public class CategoriaConverter implements Converter{

    private CategoriaDAO categoriaDAO = new CategoriaDAO();
    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String string) {
        return categoriaDAO.findById(Integer.parseInt(string));
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object o) {
        return "" + ( ( Categoria ) o ).getIdCategoria();
    }
    
}
