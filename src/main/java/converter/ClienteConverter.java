package converter;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import dao.ClienteDAO;
import model.Cliente;


@FacesConverter("clienteConverter")
public class ClienteConverter implements Converter{
    
    private ClienteDAO clienteDAO = new ClienteDAO();
    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String string) {
        return clienteDAO.findById(Integer.parseInt(string));
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object o) {
        return "" + ((Cliente) o).getIdCliente();
    }
    
}
