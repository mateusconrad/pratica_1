package converter;

import dao.UsuarioDAO;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import model.Usuario;

@FacesConverter("usuarioConverter")
public class UsuarioConverter implements Converter{
    
    private UsuarioDAO usuarioDAO = new UsuarioDAO();
    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String string) {
        return usuarioDAO.findById(Integer.parseInt(string));
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object o) {
        return "" + ( ( Usuario ) o ).getIdUsuario();
    }
    
}
