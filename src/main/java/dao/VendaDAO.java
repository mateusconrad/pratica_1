package dao;

import config.HibernateUtil;
import java.util.List;
import org.hibernate.Session;
import model.Venda;
import org.hibernate.Hibernate;

public class VendaDAO {
    private Session session;
    public VendaDAO(){
        session = HibernateUtil.getSessionFactory().openSession();
    }
    public void insert(Venda venda){
        session.getTransaction().begin();
        session.save(venda);
        session.getTransaction().commit();
    }
    public List<Venda> findAll(){
        return session.createQuery("select id from Venda").list();
    }
    public Venda findById(int id){
        return session.get(Venda.class, id );
    }
}
