
package dao;
import config.HibernateUtil;
import java.util.List;
import model.Condicional;
import org.hibernate.Session;

public class CondicionalDAO {

    private Session session;

    public CondicionalDAO() {
        session = HibernateUtil.getSessionFactory().openSession();
    }

    public void insert(Condicional condicional) {
        session.getTransaction().begin();
        session.save(condicional);
        session.getTransaction().commit();
    }

    public List<Condicional> findAll() {
        return session.createQuery("from Condicional as c ").list();//não sei se fica as "c" 
    }
}
