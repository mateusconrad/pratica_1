package dao;
import config.HibernateUtil;
import java.util.List;
import model.Cliente;
import org.hibernate.Session;

public class ClienteDAO {  
    private Session session;
    public ClienteDAO(){
        session = HibernateUtil.getSessionFactory().openSession();
    }
    public void insert(Cliente cliente){
        session.getTransaction().begin();
        session.save(cliente);
        session.getTransaction().commit();
    }
    public void delete(Cliente cliente) {
        session.getTransaction().begin();
        session.remove(cliente);
        session.getTransaction().commit();
    }
    public List<Cliente> findAll(){
        return session.createQuery("select c from Cliente c").list(); //não sei se fica as "c" 
    }
    public Cliente findById(int id){
        return session.get(Cliente.class, id );
    }
}

