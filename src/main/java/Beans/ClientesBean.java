package Beans;

import dao.ClienteDAO;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import model.Cliente;

@Named(value = "clientesBean")
@RequestScoped
public class ClientesBean implements Serializable{
    private Cliente cliente =  new Cliente();
    private ClienteDAO clienteDAO = new ClienteDAO();
    private List<Cliente> clientes = new ArrayList();
    private int idCliente;
    private String nome;
    public ClientesBean(){        
    }
    public void insertClientes(){
        clienteDAO.insert(cliente);
        this.cliente  = new Cliente();
        this.clientes = clienteDAO.findAll();            
    }
     public void removeAction(Cliente cli) {
        clienteDAO.delete(cli);
        this.clientes = clienteDAO.findAll();
    }
    public void atualizarAction() {
        this.clientes = clienteDAO.findAll();
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }
    public List<Cliente> getClientes() {
        if (this.clientes == null) {
            this.clientes = clienteDAO.findAll();
        }
        return clientes;
    }

    public void setClientes(List<Cliente> clientes) {
        this.clientes = clientes;
    }

    public ClienteDAO getClienteDAO() {
        return clienteDAO;
    }

    public void setClienteDAO(ClienteDAO clienteDAO) {
        this.clienteDAO = clienteDAO;
    }

    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
