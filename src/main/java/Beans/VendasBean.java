package Beans;
import dao.VendaDAO;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import model.Venda;

@Named(value = "vendasBean")
@RequestScoped
public class VendasBean implements Serializable {
    private Venda venda = new Venda();
    private VendaDAO vendaDAO = new VendaDAO();
    private List<Venda> vendas = new ArrayList();
    private int idCliente;
    private int idUsuario;
    private int idProduto;
    
    public VendasBean(){    
    }
  
    public void insertVendas(){
        vendaDAO.insert(venda);
        this.venda = new Venda();
        this.vendas = vendaDAO.findAll();
    }
    public void atualizarAction() {
        this.vendas = vendaDAO.findAll();
    }
    public Venda getVenda() {
        return venda;
    }
    public void setVenda(Venda venda) {
        this.venda = venda;
    }
    public VendaDAO getVendaDAO() {
        return vendaDAO;
    }
    public void setVendaDAO(VendaDAO vendaDAO) {
        this.vendaDAO = vendaDAO;
    }
    public List<Venda> getVendas() {
        return vendas;
    }
    public void setVendas(List<Venda> vendas) {
        this.vendas = vendas;
    }

    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }
    public int getIdProduto() {
        return idProduto;
    }

    public void setIdProduto(int idProduto) {
        this.idProduto = idProduto;
    }
}
