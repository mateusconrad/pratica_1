package Beans;

import dao.ProdutoDAO;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import model.Produto;

@Named(value = "produtosBean")
@RequestScoped
public class ProdutosBean implements Serializable{
private Produto produto = new Produto();
private ProdutoDAO produtoDAO = new  ProdutoDAO();
private List<Produto> produtos = new ArrayList();
    
    public ProdutosBean(){    
    }
    public void insertProdutos(){
        produtoDAO.insert(produto);
        this.produto = new Produto();
        this.produtos = produtoDAO.findAll();
    }
    public void atualizarAction() {
        this.produtos = produtoDAO.findAll();
    }
    public Produto getProduto() {
        return produto;
    }
    public void setProduto(Produto produto) {
        this.produto = produto;
    }
    public List<Produto> getProdutos() {
        return produtos;
    }
    public void setProdutos(List<Produto> produtos) {
        this.produtos = produtos;
    }
    public ProdutoDAO getProdutoDAO() {
        return produtoDAO;
    }
    public void setProdutoDAO(ProdutoDAO produtoDAO) {
        this.produtoDAO = produtoDAO;
    }
            
}
