package Beans;

import dao.UsuarioDAO;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;//import javax.enterprise.context.SessionScoped;
import model.Usuario;

@Named(value = "usuarioBean")
@RequestScoped
public class UsuarioBean implements Serializable{
    private Usuario usuario = new Usuario();
    private UsuarioDAO usuarioDAO = new UsuarioDAO();
    private List<Usuario> usuarios = new ArrayList();
    
    public UsuarioBean(){
    }
    
    public void insertUsuario(){
        usuarioDAO.insert( usuario );
        this.usuario = new Usuario();
        this.usuarios = usuarioDAO.findAll();
    }
    public void atualizarAction() {
        this.usuarios = usuarioDAO.findAll();
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public UsuarioDAO getUsuarioDAO() {
        return usuarioDAO;
    }

    public void setUsuarioDAO(UsuarioDAO usuarioDAO) {
        this.usuarioDAO = usuarioDAO;
    }

    public List<Usuario> getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(List<Usuario> usuarios) {
        this.usuarios = usuarios;
    }
}
