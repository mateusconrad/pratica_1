package Beans;

import dao.CondicionalDAO;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import model.Condicional;

@Named(value = "conficionalBean")
@RequestScoped
public class CondicionalBean implements Serializable{
    private Condicional condicional = new Condicional();
    private CondicionalDAO condicionalDAO = new CondicionalDAO();
    private List<Condicional> condicionais =  new ArrayList();

    public CondicionalBean(){
        
    }
    public void insertCondicional(){
        condicionalDAO.insert(condicional);
        this.condicional = new Condicional();
        this.condicionais = condicionalDAO.findAll();
    }

    public Condicional getCondicional() {
        return condicional;
    }

    public void setCondicional(Condicional condicional) {
        this.condicional = condicional;
    }

    public CondicionalDAO getCondicionalDAO() {
        return condicionalDAO;
    }

    public void setCondicionalDAO(CondicionalDAO condicionalDAO) {
        this.condicionalDAO = condicionalDAO;
    }

    public List<Condicional> getCondicionais() {
        return condicionais;
    }

    public void setCondicionais(List<Condicional> condicionais) {
        this.condicionais = condicionais;
    }



};
