package Beans;

import dao.CategoriaDAO;
import javax.inject.Named;//import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import model.Categoria;


@Named(value = "categoriaBean")
@RequestScoped
public class CategoriaBean implements Serializable {
    private Categoria categoria = new Categoria();
    private CategoriaDAO categoriaDAO = new CategoriaDAO();
    private List<Categoria> categorias = new ArrayList();    
  
    public CategoriaBean() {
    }
    
    public void insertCategoria(){
        categoriaDAO.insert( categoria );
        this.categoria = new Categoria();
        this.categorias = categoriaDAO.findAll();
    }
    public void atualizarAction() {
        this.categorias = categoriaDAO.findAll();
    }
    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

    public CategoriaDAO getCategoriaDAO() {
        return categoriaDAO;
    }

    public void setCategoriaDAO(CategoriaDAO categoriaDAO) {
        this.categoriaDAO = categoriaDAO;
    }

    public List<Categoria> getCategorias() {
        return categorias;
    }

    public void setCategorias(List<Categoria> categorias) {
        this.categorias = categorias;
    }
}